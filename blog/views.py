from django.shortcuts import render

# Create your views here.
def blog(req):
    return render(req, 'blog/blog.html', { 'html': '<h1>Header One</h1><p>This is a paragraph.</p>' })
